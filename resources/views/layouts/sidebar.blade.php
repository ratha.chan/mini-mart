@extends('layouts.app')
@section('page_style')
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
@endsection
@section('content')
@include('layouts.header')
	<!-- Navigation
	================================================== -->

	<!-- Responsive Navigation Trigger -->
	<a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

	<div class="dashboard-nav">
		<div class="dashboard-nav-inner">

			<ul data-submenu-title="Administraction" class="main-title">
				<li><a><i class="sl sl-icon-layers"></i> IT Department</a>
					<ul>
                        <li><a href="dashboard-my-listings.html">Campany Information</a></li>
						<li><a href="dashboard-my-listings.html">User Role</a></li>
						<li><a href="dashboard-my-listings.html">User </a></li>
                        <li><a href="dashboard-my-listings.html">Restore & Backup Database</a></li>
					</ul>
				</li>
				<li><a><i class="sl sl-icon-layers"></i>Point of Sale</a>
					<ul>
						<li><a href="dashboard-my-listings.html">Customer</a></li>
						<li><a href="dashboard-my-listings.html">Vender</a></li>
						<li><a href="dashboard-my-listings.html">Item Master</a></li>
                        <li><a href="dashboard-my-listings.html">Unit of Measure</a></li>
                        <li><a href="dashboard-my-listings.html">Payment Method</a></li>
					</ul>
				</li>
			</ul>
            <ul data-submenu-title="Inventory Control" class="main-title">
                <li><a href="dashboard-my-listings.html">Dashboard</a></li>
                <li><a><i class="sl sl-icon-layers"></i>Report</a>
					<ul>
						<li><a href="dashboard-my-listings.html">Popular Item sale</a></li>
						<li><a href="dashboard-my-listings.html">Item Expired</a></li>
						<li><a href="dashboard-my-listings.html">Item out Stock</a></li>
                        <li><a href="dashboard-my-listings.html">Earning</a></li>
					</ul>
				</li>
			</ul>
            <ul data-submenu-title="Account Receivable" class="main-title">
                <li><a href="dashboard-my-listings.html">Sale Invoice</a></li>
                <li><a href="dashboard-my-listings.html">Discount</a></li>
                <li><a href="dashboard-my-listings.html">Sale Credit</a></li>
			</ul>
            <ul data-submenu-title="Account">
				<li><a href="dashboard-my-profile.html"><i class="sl sl-icon-user"></i> My Profile</a></li>
				<li><a data-logout="{{ route('logout') }}" data-method="POST" ><i class="sl sl-icon-power"></i> Logout</a>
                </li>
			</ul>
		</div>

	</div>
	<!-- Navigation / End -->

@endsection

@section('page_script')
<script type="text/javascript">
 const base_url = $('meta[name="base-url"]').attr('content');

    $(document).ready(function(){

       $('.btn-logout').click(function(){
            let route_logout = $(this).data("logout");
            console.log(route_logout)
            let method       = $(this).data("method");
            console.log('hello')
            $.ajax({
            dataType: "json",
            url: route_logout,
            type : 'POST',
            data : {_token : '{{csrf_token()}}'},
            timeout : 2000,
            success : function(r){

            }
        });
        });
    });
</script>
@endsection
